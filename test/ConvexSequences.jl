using Test
using Sequences: convexsequence, ConvexSequence, LogConvexSequence

using Random

# initialize random values
Random.seed!(377642186851713398)

# convex sequence

## random convexity
η = rand()
## random end position
xₙ = 10
## random start value
a₀ = rand()

## initialize convex sequence with lower convexity
a = convexsequence(η, xₙ, a₀, 10a₀)
## test end point
@test a(xₙ) ≈ 10a₀
## η > 0 -> lower convex
@test a(0.5xₙ) < 5.5a₀
## initiallize convex sequence with lower convexity
a = convexsequence(-η, xₙ, a₀, 10a₀)
## η < 0 -> upper convex
@test a(0.5xₙ) > 5.5a₀

## initialize convex sequence with lower convexity
a = convexsequence(η, xₙ, a₀, 0.2a₀)
## test end point
@test a(xₙ) ≈ 0.2a₀
## η > 0 -> lower convex
@test  a(0.5xₙ) < 0.6a₀
## initiallize convex sequence with lower convexity
a = convexsequence(-η, xₙ, a₀, 0.2a₀)
## η < 0 -> upper convex
@test a(0.5xₙ) > 0.6a₀

## log convex sequence

## initialize log convex sequence
a = convexsequence(-1, xₙ, a₀, 10a₀)
## test end point
@test a(xₙ) ≈ 10a₀
## η < 0 -> upper convex
@test a(0.5xₙ) > 5.5a₀
## test dispatch
@test typeof(convexsequence(1, xₙ, a₀, 0.2a₀)) <: LogConvexSequence
@test typeof(convexsequence(-1, xₙ, a₀, 0.2a₀)) <: ConvexSequence