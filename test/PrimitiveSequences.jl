using Test
using Sequences: sequence, discrete

using Random

# initialize random values
Random.seed!(5070973882304590910)

# constant sequence

## random constant value
a = rand()
## initialize constant sequence
constant = sequence(a)
## test constant sequence
@test a == constant(rand())

# explicit sequence

## random vector
a = rand(5)
## initialize explicit sequence
explicit = sequence(a)
## test explicit sequence
@test a == explicit.(0:4)
## test tail of explicit sequence
@test last(a) == explicit(100)

# discrete sequence

## random vector
a = rand(5)
## initialize discrete explicit sequence
explicit = discrete(sequence(a))
## test discrete sequence
@test round.(a) == explicit.(0:4)