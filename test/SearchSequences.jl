using Test
using Sequences: backtrack, forwardsearch

using Base.Iterators: take

# Create backtracking sequence
sequence = backtrack(0.5, 10)

# Iterate over it and collect sequence (at most 10)
x = [x for x in take(sequence, 10)]

# Check if each pair in sequence follows constant multiplier modifier
@test all(i -> x[i]/x[i-1] ≈ 0.5, eachindex(x)[2:end])

# Last element should return maximum of seqeuence
@test last(sequence) == 10

# Create forwardsearch sequence
sequence = forwardsearch(1.1, 1, 10)

# Iterate over it and collect sequence
x = [x for x in sequence]

# Check if each pair except last (due to maximum) follows constant multiplier modifier
@test all(i -> x[i+1]/x[i] ≈ 1.1, eachindex(x)[1:end-2])

# Last element should return maximum of seqeuence
@test last(sequence) == 10