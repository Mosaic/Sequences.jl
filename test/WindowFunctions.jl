using Test
using Sequences: plancktaper

using Random

# initialize random values
Random.seed!(4196491434364056818)

# Planck-taper window

## random taper
ϵ = rand()

## initialize Planck-taper window
a = plancktaper(ϵ)
## test end point
@test isone(a(ϵ))
## test middle
@test a(0.5ϵ) ≈ 0.5

## generate random samples
x = rand(5) * ϵ
## evaluate window
y = a.(x)
## compare results
@test y ≈ [0.6747216621099269, 0.9341856106621989, 0.607554509916609, 0.8923121912584815, 0.8243974550934482]