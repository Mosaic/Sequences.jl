using Test
using TestItemRunner

using Random

@testitem "Primitive sequences" tags=[:unit, :primitive] begin
	include("PrimitiveSequences.jl")
end
@testitem "Decay sequences" tags=[:unit, :decay] begin
	include("DecaySequences.jl")
end
@testitem "Convex sequences" tags=[:unit, :convex] begin
	include("ConvexSequences.jl")
end
@testitem "Search sequences" tags=[:unit, :search] begin
	include("SearchSequences.jl")
end
@testitem "Window functions" tags=[:unit, :window] begin
	include("WindowFunctions.jl")
end
@run_package_tests filter=ti->(:unit in ti.tags)