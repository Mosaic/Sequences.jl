using Test
using Sequences: naturaldecay, exponentialdecay, discrete

using Random

# initialize random values
Random.seed!(8706528006123836946)

# natural sequence

## random start value
a₀ = 10rand()
## initialize natural sequence
a = naturaldecay(10, a₀)
## test end point
@test a(10) ≈ 1

## random exponential basis
b = rand()
## random point
x = rand()
## initialize exponential natural sequence
a = exponentialdecay(b, a₀)
## test end point
@test a(1) ≈ a₀ * b
## test exponential sequence at random point
@test a(x) ≈ a₀ * b^x

# discrete natural sequence

## initialize discrete natural sequence
a = naturaldecay(10, a₀) |> discrete
## test end point
@test iszero(a(10))