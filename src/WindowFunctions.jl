# Copyright (C) 2024 Dominik Itner
# This file is part of Sequences.jl.
# License: LGPL-3.0-or-later

"Supertype for window functions."
abstract type WindowFunction <: Sequence end

# Implementations

## Planck-taper window function

"""
	PlanckTaper(ϵ)

Define a continuous window function `w(x)` with `x ∈ ℝ` based on the Planck-taper window function.
"""
struct PlanckTaper{T} <: WindowFunction
	ϵ::T
end

"""
	plancktaper(ϵ)

Generate a `PlanckTaper` `WindowFunction`.

See also [`PlanckTaper`](@ref).
"""
plancktaper(ϵ) = PlanckTaper(ϵ)

"Evaluate Planck-taper window function `w(x) = (1 + exp(ϵ/x - ϵ/(ϵ-x)))⁻¹` with `x ∈ ℝ`."
(w::PlanckTaper)(x) =
	inv(1 + exp(w.ϵ/x - w.ϵ/(w.ϵ-x)))