# License
#
# Copyright (C) 2024 Dominik Itner
#
# Sequences.jl is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Sequences.jl is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with Sequences.jl.  If not, see <http://www.gnu Lesser.org/licenses/>.
#
# A copy of the GNU Lesser General Public License has deliberately not been included to circumvent unnecessary traffic due to the high degree of modularity and interoperability of packages in Mosaic.jl. The license can instead be found at <https://codeberg.org/Mosaic/License>.

module Sequences

# Supertype

"A `Sequence` behaves like a `Function`. Given either an input `i ∈ ℕ₀` or `x ∈ ℝ`, return the corresponding value in the sequence."
abstract type Sequence <: Function end

# External interface

"Initialize a `Sequence`."
function sequence end

"In general, initialize any `a<:Sequence` by simply returning itself."
sequence(a::Sequence) = a

# Discrete sequences

"Discretize a `Sequence`."
function discrete end

"In general, do not modify underlying `Sequence` when discretizing it."
discrete(a::Sequence) = Discrete(a)

"Special case of sequences where `i, aᵢ ∈ ℕ₀`. Any `Sequence` can be converted into a `Discrete{Sequence}`."
struct Discrete{T<:Sequence} <: Sequence
	parent::T
end

"In general, if a `Discrete{Sequence}` is called, round result of parent s.t. output is an `Int`."
(a::Discrete{<:Sequence})(i) = round(Int, a.parent(i))

"If a `Sequence` is already `Discrete`, do not discretize it again."
discrete(a::Discrete) = a

# Implementations

## Constant sequence

"Define a sequence with a constant value."
struct ConstantSequence{T} <: Sequence
	val::T
end

"Initialize a `val::Number` as a `ConstantSequence`."
sequence(val::Number) = ConstantSequence(val)

"Return constant value."
(a::ConstantSequence)(_) = a.val

# "No rounding is necessary if it is a `Discrete{ConstantSequence{T}}` with `T<:Integer`."
(a::Discrete{<:ConstantSequence{<:Integer}})(i) = a.parent(i)

## Explicit sequence

"Define a sequence via specific values."
struct ExplicitSequence{N,T,A} <: Sequence
	tail::T
	sequence::A
	# constructor
	ExplicitSequence{N}(tail::T, sequence::A) where {N,T,A} = new{N,T,A}(tail, sequence)
end

"Initialize a `Tuple{Vararg}` or `AbstractVector` as a `ExplicitSequence`."
sequence(sequence::Union{Tuple{Vararg}, AbstractVector}, tail=last(sequence)) =
	ExplicitSequence{length(sequence)}(tail, sequence)

"Given an explicit sequence `aᵢ` with '|aᵢ| = n+1`, given `i ∈ ℕ₀` return `aᵢ` or `aₙ` if `i > n+1`."
(a::ExplicitSequence{N})(i::Integer) where N =
	i < N ? a.sequence[i+1] : a.tail

## Monotonic sequences

abstract type MonotonicSequence <: Sequence end

### Decaying monotonic sequences

include("DecaySequences.jl")

### Parametric monotonic sequences

include("ParametricSequences.jl")

## Iterable sequences

abstract type IterableSequence <: Sequence end

### Search sequences

include("SearchSequences.jl")

## Other sequences or functions

### Window functions

include("WindowFunctions.jl")

end # Sequences