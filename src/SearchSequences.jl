# Copyright (C) 2024 Dominik Itner
# This file is part of Sequences.jl.
# License: LGPL-3.0-or-later

# Supertype

abstract type SearchSequence <: IterableSequence end

# Implementations

## Back tracking

struct BackTracking{F,T} <: SearchSequence
	modifier::F
	maximum::T
end

### Instantiate

"""
	backtrack(f, x₀)

Construct an iterable sequence with maximum `x₀` s.t. `xᵢ₊₁ = f(xᵢ)`.
"""
function backtrack(modifier::Function, maximum)
	# figure out return type of modifier
	T = typeof(modifier(maximum))
	# instantiate backtracking by converting maximum
	BackTracking(modifier, convert(T, maximum))
end

"""
	backtrack(α, x₀)

Construct an iterable sequence with maximum `x₀` s.t. `xᵢ₊₁ = α * xᵢ` with `α ∈ (0, 1)`.
"""
function backtrack(modifier::Number, maximum)
	@assert (0 < modifier < 1) "Modifier must be between `0` and `1`."
	backtrack(x -> modifier * x, maximum)
end

### Extend external interface

Base.last(sequence::BackTracking) = sequence.maximum

function Base.iterate(sequence::BackTracking, xᵢ=sequence.maximum)
	# compute next value
	xᵢ₊₁ = sequence.modifier(xᵢ)
	# return current state to iteration and next state for next iteration
	return xᵢ, xᵢ₊₁
end

Base.eltype(::BackTracking{<:Function,T}) where T = T

Base.IteratorSize(::BackTracking) = Base.IsInfinite()

## Forward Search

struct ForwardSearch{F,T} <: SearchSequence
	modifier::F
	start::T
	maximum::T
end

### Instantiate

"""
	forwardsearch(f, x₀, xₙ)

Construct an iterable sequence with start `x₀` and maximum `xₙ` s.t. `xᵢ₊₁ = min(f(xᵢ), xₙ)`.
"""
function forwardsearch(modifier, start, maximum)
	# figure out return type of modifier
	T = typeof(modifier(start))
	# instantiate forward search by converting start and maximum
	ForwardSearch(modifier, convert(T, start), convert(T, maximum))
end

"""
	forwardsearch(α, x₀, xₙ)

Construct an iterable sequence with start `x₀` and maximum `xₙ` s.t. `xᵢ₊₁ = min(α * xᵢ, xₙ)` with `α > 1`.
"""
function forwardsearch(modifier::Number, start, maximum)
	@assert modifier > 1 "Modifier must be greater than `1`."
	forwardsearch(x -> modifier * x, start, maximum)
end

### Extend external interface

Base.last(sequence::ForwardSearch) = sequence.maximum

function Base.iterate(sequence::ForwardSearch, xᵢ=sequence.start)
	# fetch maximum
	xₙ = sequence.maximum
	# return maximum itself if sequence has reached value beyond maximum
	xᵢ > xₙ && return (xₙ, xₙ)
	# return nothing if this step is equivalent to maximum
	xᵢ ≡ xₙ && return nothing
	# compute next value
	xᵢ₊₁ = sequence.modifier(xᵢ)
	# return current state to iteration and next state for next iteration
	return xᵢ, xᵢ₊₁
end

Base.eltype(::ForwardSearch{<:Function,T}) where T = T

Base.IteratorSize(::ForwardSearch) = Base.SizeUnknown()