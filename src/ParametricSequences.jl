# Copyright (C) 2024 Dominik Itner
# This file is part of Sequences.jl.
# License: LGPL-3.0-or-later

"Supertype for parametric `Sequence`s."
abstract type ParametricSequence <: MonotonicSequence end

# Implementations

## Polynomial sequence

"""
`ConvexSequence`s are strictly monotonic sequences (both decreasing and increasing) with adjustable convexity.

Convexity can be set by user via parameter `η`:
- η > 0 → lower convex
- η < 0 → upper convex

Monotonicity is guaranteed via:
- `a′(x) > 0` → increase
- `a′(x) < 0` → decrease

Convexity is adjustable via:
- `a″(x) < 0` → upper convex
- `a″(x) > 0` → lower convex

The simple polynomial integral forms the basis with
`a′(x) = α xⁿ ⇒ a(x) = ∫ α xⁿ = α/(1+η) x^(1+η) + β, η ≠ -1`

The special case `η = -1` leads to a logarithmic expression (see `LogConvexSequence`).
"""
struct ConvexSequence{N,F,I} <: ParametricSequence
	η::N
	α::F
	β::I
end

"""
	convexsequence(η, xₙ, a₀, aₙ)

Compute `α` and `β` s.t. `a(0) = a₀` and `a(xₙ) = aₙ` for `η ∈ ℝ`.
With `η = -1`, compute parameters for a `LogConvexSequence`.
"""
function convexsequence(η, xₙ, a₀, aₙ)
	# switch meaning (sign) of η depending on relation of a₀ to aₙ
	η = (1 - 2(a₀ > aₙ)) * η
	# distinguish between standard polynomial or logarithmic integral
	if isone(-η)
		logconvexsequence(xₙ, a₀, aₙ)
	else
		# compute intermediary value
		aᵦ= (aₙ-a₀) / (float(1+xₙ)^(1+η)-1)
		# compute factor
		α = (1+η) * aᵦ
		# compute summand
		β = a₀ - aᵦ
		# done
		ConvexSequence(η, α, β)
	end
end

"Compute convex sequence via `a(x) = α/(1+η) x^(1+η) + β, η ≠ -1`."
(a::ConvexSequence)(x) =
	a.α*(1+x)^(1+a.η) / (1+a.η) + a.β

"""
`LogConvexSequence` is a special case of a `ConvexSequence` with `η = -1`.
The integral (see `ConvexSequence`) then evaluates to (after modification of parameters):
`a(x) = ∫ α x⁻¹  = α ln(β x)`
"""
struct LogConvexSequence{F,I} <: ParametricSequence
	α::F
	β::I
end

"""
	logconvexsequence(xₙ, a₀, aₙ)

Compute `α` and `β` s.t. `a(0) = a₀` and `a(xₙ) = aₙ` for `η = -1` (see `convexsequence``).
"""
function logconvexsequence(xₙ, a₀, aₙ)
	α = (aₙ-a₀) / log(1+xₙ)
	β = (1+xₙ)^inv(aₙ/a₀-1)
	LogConvexSequence(α, β)
end

"Compute log convex sequence via `a(x) = α ln(β x)`."
(a::LogConvexSequence)(x) =
	a.α * log(a.β*(1+x))