# Copyright (C) 2024 Dominik Itner
# This file is part of Sequences.jl.
# License: LGPL-3.0-or-later

"Supertype for strictly monotonically decreasing `Sequence`s."
abstract type DecaySequence <: MonotonicSequence end

# "Floor any `Discrete{DecaySequence}`."
(a::Discrete{<:DecaySequence})(i) = floor(Int, a.parent(i))

# Implementations

## Natural decay

"""
	NaturalDecay(γ, a₀)

Define a sequence `a(x) = a₀ * exp(γ * x)` with `γ ∈ (0, 1)` s.t. `a(0) = a₀`.
"""
struct NaturalDecay{D,T} <: DecaySequence
	γ::D
	a₀::T
end

"""
	naturaldecay(xₙ, a₀)

Compute decay factor `γ` s.t. `a(0) = a₀` and `a(xₙ) = 1`.
"""
naturaldecay(xₙ, a₀) =
	NaturalDecay(-log(a₀) / xₙ, a₀)

"""
	exponentialdecay(b, a₀)

Compute decay factor `γ` s.t. `a(0) = a₀` and `a(x) = a₀ * bˣ` with `b(γ)`.
"""
exponentialdecay(b, a₀) =
	NaturalDecay(log(b), a₀)

"Compute natural decay via `a(x) = a₀ * exp(γ * x)`."
(a::NaturalDecay)(x) =
	a.a₀ * exp(a.γ * x)

"Discretize a `NaturalDecay` by subtracting `eps(T)` from decay factor `γ::T` to guarantee `⌊ a₀ * exp(γ * iₙ) ⌋ = 0`."
discrete(a::NaturalDecay{T}) where T =
	Discrete(NaturalDecay(a.γ - eps(T), a.a₀))